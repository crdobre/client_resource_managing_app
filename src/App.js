import React, {Component} from 'react';

class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            isLoaded: false,
        }
    }

    componentDidMount() {
        fetch('http://localhost:8080/api/projects')
        .then(res => res.json())
        .then(json => {
                this.setState({
                    isLoaded:true,
                    projects: json,
                })
        });

    }

    render() {
        var { isLoaded, projects} = this.state;

        if (!isLoaded){
            return <div> Loading..</div>
        }
        else {
        return (
            <div className="App">
                    <ul>
                        {projects.map(project => (
                            <li key={project.projectid}>
                                Code: {project.code}
                            </li>
                        ))};

                    </ul>
            </div>
        );

        }
    }


}

export default App;
