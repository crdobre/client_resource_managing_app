import React from "react"
import "./Navbar.css"
//import Logo from "../../assets/images/logo.svg"

const Navbar = ({ sticky }) => (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" >EN-EL</a>
    <button type="button" class="navbar-toggler" data-toggle="collapse"
        data-target="#hello-navbar-collapse" aria-expanded="false">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="hello-navbar-collapse">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link" >Projects</a></li>

            <li class="nav-item"><a class="nav-link" >Personnel</a></li>

            <li class="nav-item"><a class="nav-link">Dashboard</a></li>

        </ul>
         
    </div>
</nav>

)
export default Navbar